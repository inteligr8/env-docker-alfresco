
## Purpose

This Git Repository intends to represent environments in Docker Compose.  All environments are effectively a derivative of other environments.  The original environment is the environment represented by the `base` branch.  All derivative environments are represented by other branches.  Those branches are named in the format `{core}.{parent}`.

